package com.example.ACAP.ui.ChatActivity;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.ACAP.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

public class ChatAdapter  extends RecyclerView.Adapter<ChatAdapter.ChatActivityViewHolder> {

    private Context context;
    ArrayList<Chat> chatList;
    ArrayList<String> imgURL;

    // Firebase
    FirebaseUser firebaseUser;

    public static final int MSG_TYPE_LEFT = 0;
    public static final int MSG_TYPE_RIGHT = 1;

    public static final int MEDIA_TYPE_RIGHT = 2;
    public static final int MEDIA_TYPE_LEFT = 3;

    public ChatAdapter(Context context, ArrayList<Chat> chatList, ArrayList<String> imgURL) {
        this.context = context;
        this.chatList = chatList;
        this.imgURL = imgURL;


    }

    @NonNull
    @Override
    public ChatAdapter.ChatActivityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView;
        if (viewType == MSG_TYPE_RIGHT)
            layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item_right, parent, false);
        else if (viewType == MEDIA_TYPE_RIGHT)
            layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_media_right, parent, false);
        else if (viewType == MSG_TYPE_LEFT)
            layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item_left, parent, false);
        else
            layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_media_left, parent, false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutView.setLayoutParams(lp);
        return new ChatActivityViewHolder(layoutView, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull  ChatAdapter.ChatActivityViewHolder holder, int position) {

        Chat chat = chatList.get(position);
        if (holder.show_message != null)
            holder.show_message.setText(chat.getMessage());
        else
            Glide.with(context).load(Uri.parse(chat.getMedia())).into(holder.show_media);

        if (imgURL.get(position).equals("default")) {
            holder.userImage.setImageResource(R.mipmap.ic_launcher);
        }
        else {
            Glide.with(context).load(imgURL.get(position)).into(holder.userImage);
        }
    }

    @Override
    public int getItemCount() {
        return chatList.size();

    }


    public class ChatActivityViewHolder extends RecyclerView.ViewHolder {
        public TextView show_message;
        public ImageView show_media;
        public ImageView userImage;
        public ChatActivityViewHolder(View view,int type) {
            super(view);
            if (type == MSG_TYPE_LEFT || type == MSG_TYPE_RIGHT)
                show_message = view.findViewById(R.id.text_chat_item);
            else
                show_media = view.findViewById(R.id.media_chat_item);
            userImage = view.findViewById(R.id.user_chat_item);
        }
    }

    @Override
    public int getItemViewType(int position) {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (chatList.get(position).getSender().equals(firebaseUser.getUid())) {
            if (chatList.get(position).getMessage() != null)
                return MSG_TYPE_RIGHT;
            else
                return MEDIA_TYPE_RIGHT;
        }
        else {
            if (chatList.get(position).getMessage() != null)
                return MSG_TYPE_LEFT;
            else
                return MEDIA_TYPE_LEFT;
        }
    }
}
