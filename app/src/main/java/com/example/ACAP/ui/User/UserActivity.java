package com.example.ACAP.ui.User;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ACAP.R;
import com.example.ACAP.ui.welcome.WelcomeActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class UserActivity extends AppCompatActivity {

    private FirebaseAuth auth = FirebaseAuth.getInstance();
    final FirebaseUser currentFirebaseUse = auth.getInstance().getCurrentUser();;
    DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("MyUsers");
    private RecyclerView userListView;
    private RecyclerView.Adapter userListAdapter;
    private RecyclerView.LayoutManager userListManager;

    Button logoutButton;
    TextView username;
    ImageView userImage;

    ArrayList<User> userList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        username = findViewById(R.id.usernameUser);
        userImage = findViewById(R.id.userImage);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    User user = dataSnapshot.getValue(User.class);
                    assert user != null;
                    if (user.getId().equals(currentFirebaseUse.getUid())) {
                        username.setText(user.getUsername());
                        if (user.getImageURL().equals("default")) {
                           userImage.setImageResource(R.mipmap.ic_launcher);
                        }
                        else {
                            Glide.with(getApplicationContext()).load(user.getImageURL()).into(userImage);
                        }
                        break;
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });

        logoutButton = findViewById(R.id.logoutUser);
        userList = new ArrayList<>();


        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(UserActivity.this, WelcomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                auth.signOut();
                finish();
            }

        });


        userListView = findViewById(R.id.userList);
        userListView.setNestedScrollingEnabled(false);
        userListView.setHasFixedSize(true);
        userListManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        userListView.setLayoutManager(userListManager);
        userListAdapter = new UserListAdapter(getApplicationContext(), userList);
        userListView.setAdapter(userListAdapter);

        readUsers();
        //initializeRecyclerView();
    }

    private void readUsers() {
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                userList.clear();

                for(DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    User user = dataSnapshot.getValue(User.class);

                    assert user != null;
                    if (!user.getId().equals(currentFirebaseUse.getUid())) {
                        userList.add(user);
                       // Log.d("working",user.getUsername());
                        userListAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}