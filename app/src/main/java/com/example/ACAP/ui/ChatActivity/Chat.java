package com.example.ACAP.ui.ChatActivity;

public class Chat {

    private String sender;
    private String receiver;
    private String message;
    private String media;

    public Chat(String message, String receiver, String sender, String media) {
        this.message = message;
        this.receiver = receiver;
        this.sender = sender;
        this.media = media;


    }

    public Chat() {

    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMedia() { return media; }

    public void setMedia(String media) {
        this.media = media;
    }
}
