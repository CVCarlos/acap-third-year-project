package com.example.ACAP.ui.register;

import androidx.annotation.NonNull;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.ACAP.R;
import com.example.ACAP.ui.login.LoginActivity;
import com.example.ACAP.ui.welcome.WelcomeActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

    EditText usernameEditText;
    EditText emailEditText;
    EditText passwordEditText;
    Button registerButton;
    ProgressBar loadingProgressBar;

    private FirebaseAuth auth = FirebaseAuth.getInstance();
    private DatabaseReference myRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        usernameEditText = findViewById(R.id.username_register);
        emailEditText = findViewById(R.id.email_register);
        passwordEditText = findViewById(R.id.password_register);
        registerButton = findViewById(R.id.register);
        loadingProgressBar = findViewById(R.id.loading);


        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean goRegister = true;
                String username_text = usernameEditText.getText().toString();
                String email_text = emailEditText.getText().toString();
                String password_text = passwordEditText.getText().toString();

                if (TextUtils.isEmpty(username_text) || username_text.length() > 10) {
                    usernameEditText.setError("Username is not valid");
                    goRegister = false;
                }
                if (TextUtils.isEmpty(email_text) || !isEmailValid(email_text)) {
                    emailEditText.setError("Email is not valid");
                    goRegister = false;
                }
                if (TextUtils.isEmpty(password_text)) {
                    passwordEditText.setError("Password is not valid");
                    goRegister = false;
                }
                if (goRegister)
                    registration(username_text,email_text,password_text);
            }
        });
    }


    private boolean isEmailValid(CharSequence email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void registration(final String username, String email, String password) {
        Query usernameQuery = FirebaseDatabase.getInstance().getReference().child("Users").orderByChild("username").equalTo(username);
        usernameQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.getChildrenCount() > 0)
                    Toast.makeText(RegisterActivity.this,"username already taken",Toast.LENGTH_SHORT).show();
                else {
                    auth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        FirebaseUser firebaseUser = auth.getCurrentUser();
                                        String userid = firebaseUser.getUid();

                                        myRef = FirebaseDatabase.getInstance()
                                                .getReference("MyUsers").child(userid);

                                        // HashMaps
                                        HashMap<String, String> hashMap = new HashMap<>();
                                        hashMap.put("id", userid);
                                        hashMap.put("username", username);
                                        hashMap.put("imageURL", "default");

                                        myRef.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                    auth.signOut();
                                                    startActivity(i);
                                                    finish();
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        Toast.makeText(RegisterActivity.this, "Invalid Email or Password", Toast.LENGTH_SHORT).show();
                                    }
                                 }
                            });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}