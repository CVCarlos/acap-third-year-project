package com.example.ACAP.ui.ChatActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.ACAP.R;
import com.example.ACAP.ui.User.User;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;


import java.util.ArrayList;
import java.util.HashMap;


public class ChatActivity extends AppCompatActivity {

    TextView username;
    ImageView userImage;

    RecyclerView messageVIew;
    EditText messageSend;
    Button buttonSend;
    Button buttonMediaSelect;
    ImageButton buttonMediaCancel;

    FirebaseUser firebaseUser;
    DatabaseReference myRef;
    Intent intent;

    int SELECT_IMAGE_INTENT = 1;
    ArrayList<String> mediaUriList;
    private RecyclerView mediaView;
    private RecyclerView.Adapter mediaAdapter;
    private RecyclerView.LayoutManager mediaManager;



    ArrayList<Chat> chatList;
    ArrayList<String> imageurlList;
    private RecyclerView chatView;
    private RecyclerView.LayoutManager chatManager;
    private RecyclerView.Adapter chatAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        userImage = findViewById(R.id.userImageChat);
        username = findViewById(R.id.usernameChat);

        intent = getIntent();

        String userID = intent.getStringExtra("userid");


        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        myRef = FirebaseDatabase.getInstance().getReference("MyUsers").child(userID);


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User user = snapshot.getValue(User.class);
                username.setText(user.getUsername());

                if (user.getImageURL().equals("default")) {
                    userImage.setImageResource(R.mipmap.ic_launcher);
                }
                else {
                    Glide.with(getApplicationContext()).load(user.getImageURL()).into(userImage);
                }

                receiveMessage(firebaseUser.getUid(),userID,user.getImageURL());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        messageSend = findViewById(R.id.messageWriteChat);
        buttonSend = findViewById(R.id.sendChat);

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageToSend = messageSend.getText().toString();

                DatabaseReference checkBanned = FirebaseDatabase.getInstance().getReference("banned_uids").child(firebaseUser.getUid());
                checkBanned.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot.exists()) {
                            Toast.makeText(ChatActivity.this, "Banned", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            sendMessage(firebaseUser.getUid(), userID, messageToSend.trim());
                            messageSend.setText("");
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                    }
                });
            }
        });

        buttonMediaSelect = findViewById(R.id.uploadImageChat);

        buttonMediaSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        chatList = new ArrayList<>();
        imageurlList = new ArrayList<>();

        chatView = findViewById(R.id.messageViewChat);
        chatView.setNestedScrollingEnabled(false);
        chatView.setHasFixedSize(true);
        chatManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        chatView.setLayoutManager(chatManager);
        chatAdapter = new ChatAdapter(getApplicationContext(), chatList, imageurlList);
        chatView.setAdapter(chatAdapter);

        mediaUriList = new ArrayList<>();

        mediaView = findViewById(R.id.mediaViewChat);
        mediaView.setNestedScrollingEnabled(false);
        mediaView.setHasFixedSize(true);
        mediaManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.HORIZONTAL, false);
        mediaView.setLayoutManager(mediaManager);
        mediaAdapter = new MediaAdapter(getApplicationContext(), mediaUriList);
        mediaView.setAdapter(mediaAdapter);

        buttonMediaCancel = findViewById(R.id.mediaCancel);

        buttonMediaCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaUriList.clear();
                mediaAdapter.notifyDataSetChanged();
                buttonMediaCancel.setVisibility(View.GONE);
            }
        });
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture(s)"), SELECT_IMAGE_INTENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if(resultCode == RESULT_OK)
            if(requestCode == SELECT_IMAGE_INTENT) {
                if (data.getClipData() == null)
                    mediaUriList.add(data.getData().toString());
                else
                    for(int i = 0; i < data.getClipData().getItemCount(); i++) {
                        mediaUriList.add(data.getClipData().getItemAt(i).getUri().toString());
                    }

                mediaAdapter.notifyDataSetChanged();
                buttonMediaCancel.setVisibility(View.VISIBLE);
            }
    }

    private void sendMessage(String sender, String receiver, String message) {
        if (!message.equals("") || !mediaUriList.isEmpty()) {

            DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

            HashMap<String, Object> messageMap;

            String messageID;
            DatabaseReference messageReference;

            if (!message.equals("")) {
                messageMap = new HashMap<>();
                messageMap.put("message", message);
                messageMap.put("sender", sender);
                messageMap.put("receiver", receiver);
                messageID = reference.child("Chats").push().getKey();
                messageReference = reference.child("Chats").child(messageID);
                messageReference.setValue(messageMap);
            }

            if (!mediaUriList.isEmpty()) {
                for (String mediaUri : mediaUriList) {
                    messageID = reference.child("Chats").push().getKey();
                    messageReference = reference.child("Chats").child(messageID);
                    StorageReference filePath = FirebaseStorage.getInstance().getReference().child("Chats").child(messageID);
                    messageMap = new HashMap<>();
                    UploadTask uploadTask = filePath.putFile(Uri.parse(mediaUri));
                    HashMap<String, Object> finalMessageMap = messageMap;
                    DatabaseReference finalMessageReference = messageReference;
                    uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            filePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    finalMessageMap.put("media", uri.toString());
                                    finalMessageMap.put("sender", sender);
                                    finalMessageMap.put("receiver", receiver);
                                    finalMessageReference.setValue(finalMessageMap);
                                    mediaUriList.remove(mediaUri);
                                    mediaAdapter.notifyDataSetChanged();
                                    if (mediaUriList.isEmpty()) {
                                        buttonMediaCancel.setVisibility(View.GONE);
                                    }
//                                        updateDatabaseWithNewMessage(messageReference,messageMap);
                                }
                            });
                        }
                    });
                }
            }
//            else
//                updateDatabaseWithNewMessage(messageReference,messageMap);
        }
    }

    private void receiveMessage(String current_id, String user_id, String imageurl) {

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Chats");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                chatList.clear();

                for(DataSnapshot dataSnapshot : snapshot.getChildren()) {

                    Chat chat = dataSnapshot.getValue(Chat.class);

                    assert chat != null;
                    if ((chat.getReceiver().equals(current_id) && chat.getSender().equals(user_id)) ||
                            (chat.getReceiver().equals(user_id) && chat.getSender().equals(current_id))) {

                        chatList.add(chat);
                        imageurlList.add(imageurl);
                    }

                    chatAdapter.notifyDataSetChanged();
                    chatView.scrollToPosition(chatAdapter.getItemCount() - 1);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}